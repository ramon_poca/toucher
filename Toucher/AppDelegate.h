//
//  AppDelegate.h
//  Toucher
//
//  Created by Ramon Poca on 4/1/17.
//  Copyright © 2017 ptchwrks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

