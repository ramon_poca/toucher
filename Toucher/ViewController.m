//
//  ViewController.m
//  Toucher
//
//  Created by Ramon Poca on 4/1/17.
//  Copyright © 2017 ptchwrks. All rights reserved.
//

#import "ViewController.h"
#import "TouchDetectView.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet TouchDetectView *touchDetectView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)onClear:(id)sender {
    [self.touchDetectView clearAll];
}


@end
