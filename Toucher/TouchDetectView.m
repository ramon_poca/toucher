//
//  TouchDetectView.m
//  Toucher
//
//  Created by Ramon Poca on 4/1/17.
//  Copyright © 2017 ptchwrks. All rights reserved.
//

#import "TouchDetectView.h"
#import "TouchCircleView.h"

@implementation TouchDetectView {
    NSMutableDictionary<NSValue *, TouchCircleView *> *_touches;
    TouchCircleView *_centroidView;
    IBOutlet UILabel *touchesLabel;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _touches = [NSMutableDictionary new];
    }
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    for (UITouch *touch in touches) {
        CGPoint loc = [touch locationInView:self];
        TouchCircleView *tcv = [[TouchCircleView alloc] init];
        tcv.frame = CGRectMake(0, 0, touch.majorRadius*7, touch.majorRadius*7);
        tcv.center = loc;
        [tcv setNeedsDisplay];
        [self addSubview:tcv];
        _touches[[NSValue valueWithPointer:(__bridge const void * _Nullable)(touch)]] = tcv;
    }
    
    if (_touches.count == 3) {
        if (_centroidView) {
            [_centroidView removeFromSuperview];
        }
        CGPoint center;
        CGFloat radius = [self centroidViewFromTouches:&center];
        if (radius == -NAN)
            return;
        _centroidView = [[TouchCircleView alloc] init];
        _centroidView.borderColor = [UIColor redColor];
        _centroidView.frame = CGRectMake(0, 0, 2*radius, 2*radius);
        _centroidView.center = center;
        [_centroidView setNeedsDisplay];
        [self addSubview:_centroidView];
    }
    touchesLabel.text = [NSString stringWithFormat:@"Touches: %d", _touches.count];
    [super touchesBegan:touches withEvent:event];
}

- (CGFloat) centroidViewFromTouches: (CGPoint *) center {
    if (_touches.count < 3)
        return -NAN;
    CGPoint a,b,c;
    NSArray *keys = _touches.allKeys;
    a = _touches[keys[0]].center;
    b = _touches[keys[1]].center;
    c = _touches[keys[2]].center;
    
    CGFloat A = b.x - a.x,
    B = b.y - a.y,
    C = c.x - a.x,
    D = c.y - a.y,
    E = A * (a.x + b.x) + B * (a.y + b.y),
    F = C * (a.x + c.x) + D * (a.y + c.y),
    G = 2 * (A * (c.y - b.y) - B * (c.x - b.x)),
    minx, miny, dx, dy;
    
#define MMIN(a,b,c) MIN(a,MIN(b,c))
#define MMAX(a,b,c) MAX(a,MAX(b,c))
    
    /* If the points of the triangle are collinear, then just find the
     * extremes and use the midpoint as the center of the circumcircle. */
    if(fabs(G) < 0.000001) {
        minx = MMIN(a.x, b.x, c.x);
        miny = MMIN(a.y, b.y, c.y);
        dx   = (MMAX(a.x, b.x, c.x) - minx) * 0.5;
        dy   = (MMAX(a.y, b.y, c.y) - miny) * 0.5;
        
        center->x = minx + dx;
        center->y = miny + dy;
        return sqrt(dx * dx + dy * dy);
    } else {
        center->x = (D*E - B*F) / G;
        center->y = (A*F - C*E) / G;
        dx = center->x - a.x;
        dy = center->y - a.y;
        return sqrt(dx * dx + dy * dy);
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        UIView *tcv = _touches[[NSValue valueWithPointer:(__bridge const void * _Nullable)(touch)]];
        if (tcv) {
            CGPoint loc = [touch locationInView:self];
            tcv.center = loc;
            [tcv setNeedsDisplay];
        }
    }
    if (_touches.count == 3 && _centroidView) {
        CGPoint center;
        CGFloat radius = [self centroidViewFromTouches:&center];
        if (radius == -NAN)
            return;
        _centroidView.frame = CGRectMake(0, 0, 2*radius, 2*radius);
        _centroidView.center = center;
        [_centroidView setNeedsDisplay];
    }
    touchesLabel.text = [NSString stringWithFormat:@"Touches: %d", _touches.count];
    [super touchesMoved:touches withEvent:event];

}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        UIView *tcv = _touches[[NSValue valueWithPointer:(__bridge const void * _Nullable)(touch)]];
        if (tcv) {
            [tcv removeFromSuperview];
        }
        [_touches removeObjectForKey:[NSValue valueWithPointer:(__bridge const void * _Nullable)(touch)]];
    }
    if (_touches.count < 3 && _centroidView) {
        [_centroidView removeFromSuperview];
        _centroidView = nil;
    }
    touchesLabel.text = [NSString stringWithFormat:@"Touches: %d", _touches.count];

    [super touchesEnded:touches withEvent:event];

}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        UIView *tcv = _touches[[NSValue valueWithPointer:(__bridge const void * _Nullable)(touch)]];
        if (tcv) {
            [tcv removeFromSuperview];
        }
        [_touches removeObjectForKey:[NSValue valueWithPointer:(__bridge const void * _Nullable)(touch)]];
    }
    if (_touches.count < 3 && _centroidView) {
        [_centroidView removeFromSuperview];
        _centroidView = nil;
    }
    touchesLabel.text = [NSString stringWithFormat:@"Touches: %d", _touches.count];
    [super touchesCancelled:touches withEvent:event];
}

- (void) clearAll {
    for (UIView *view in _touches.allValues) {
        [view removeFromSuperview];
    }
    [_centroidView removeFromSuperview];
    _centroidView = nil;
}

@end
