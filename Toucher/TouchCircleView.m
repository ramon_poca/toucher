//
//  TouchCircleView.m
//  Toucher
//
//  Created by Ramon Poca on 4/1/17.
//  Copyright © 2017 ptchwrks. All rights reserved.
//

#import "TouchCircleView.h"

@implementation TouchCircleView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.layer.borderWidth = 3.0;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.cornerRadius = 10;
        self.userInteractionEnabled = NO;
    }
    return self;
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    self.layer.borderColor = borderColor.CGColor;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    self.layer.cornerRadius = frame.size.width/2.0f;
}

@end
